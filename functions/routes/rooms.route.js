import express from 'express'

export const router = express.Router();

router.route('/')
  .get(async (req, res) => {
    res.status(200).json({
      message: "Rooms",
    });
  })