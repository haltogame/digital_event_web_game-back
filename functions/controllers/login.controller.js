import { User } from '../schemas/User.schema.js'
import { generateToken } from '../utils/token.js'
import sha256 from 'crypto-js/sha256.js';

export const login = async (req, res) => {
  const id = req.params.id
  const { password, token } = req.body
  try {
    if(token) {
      const user = await User.findOne({ token: token })
      if(user !== null) {
        res.status(200).json({
          user: user,
          token: token
        });
      } else {
        res.status(400).json({
          result: "wrong_token",
        });
      }
    }

    if(password) {
      const token = generateToken()
      const hashedPassword = sha256(password).toString()
      const user = await User.findOneAndUpdate({ cleanedPseudo: id, password: hashedPassword }, { token: token})
      if(user !== null) {
        res.status(200).json({
          token: token,
          user: user
        });
      } else {
        res.status(400).json({
          result: "wrong_identifier",
        });
      }
    }
  } catch (err) {
    res.status(400).json({
      message: "An error occured, please check logs for more details",
      errorMessage: err.message,
      err,
    });
  }
};