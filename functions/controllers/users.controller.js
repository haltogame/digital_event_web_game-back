import { User } from '../schemas/User.schema.js'
import { generateToken } from '../utils/token.js'
import sha256 from 'crypto-js/sha256.js';

// Objects to return on Mongo request, excludes password & token
const safeReturnedObjects = { token: 0, password: 0 }

export const createUser = async (req, res) => {
  const { cleanedPseudo, pseudo, password } = req.body;
  try {
    const token = generateToken()
    const hashedPassword = sha256(password).toString()
    const user = await new User({ cleanedPseudo: cleanedPseudo, pseudo: pseudo, password: hashedPassword, token: token, score: 0 }, safeReturnedObjects)
    await user.save()

    user.password = "blocked"

    res.status(200).json({
      user: user,
      token: token
    });
  } catch(err) {
    // Handle error
    switch (err.message) {
      case ("User validation failed: password: Path `password` is required.") :
        res.status(400).json({
          message: "Merci de saisir un mot de passe correct.",
        });
        break
      case ("User validation failed: cleanedPseudo: Path `cleanedPseudo` is required.") :
        res.status(400).json({
          message: "Merci de saisir un identifiant correct.",
        });
        break
      case ("User validation failed: pseudo: Path `pseudo` is required.") :
        res.status(400).json({
          message: "Merci de saisir un pseudo correct.",
        });
        break
      default :
        res.status(400).json({
          message: "Erreur inconnue",
        });
        break
    }
  }
};

export const getUsers = async (req, res) => {
  try {
    const users = await User.find({}, safeReturnedObjects).sort({ score: -1});
    if(req.body.order) {
      users.sort({ score: 1})
    }
    res.status(200).json({
      message: "Users retrieved",
      users: users,
    });
  } catch (err) {
    res.status(400).json({
      message: "An error occured, please check logs for more details",
      errorMessage: err.message,
      err,
    });
  }
};

export const getUser = async (req, res) => {
  const id = req.params.id
  try {
    const user = await User.find({ cleanedPseudo: id}, safeReturnedObjects);
    res.status(200).json({
      message: "Users retrieved",
      user: user,
    });
  } catch (err) {
    res.status(400).json({
      message: "An error occured, please check logs for more details",
      errorMessage: err.message,
      err,
    });
  }
};

export const updateUser = async (req, res) => {
  const id = req.params.id
  const data = req.body
  try {
    const user = await User.findOneAndUpdate({ cleanedPseudo: id}, data);
    res.status(200).json({
      message: "Users retrieved",
      user: user
    });
  } catch (err) {
    res.status(400).json({
      message: "An error occured, please check logs for more details",
      errorMessage: err.message,
      err,
    });
  }
};

export const deleteUser = async (req, res) => {
  const id = req.params.id
  try {
    const user = await User.deleteOne({cleanedPseudo: id});
    res.status(200).json({
      message: "Users deleted",
    });
  } catch (err) {
    res.status(400).json({
      message: "An error occured, please check logs for more details",
      errorMessage: err.message,
      err,
    });
  }
};